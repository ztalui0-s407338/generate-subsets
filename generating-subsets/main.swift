//
//  main.swift
//  generating-subsets
//
//  Created by Arkadiusz Żmudzin on 2019-05-29.
//  Copyright © 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

let setSize = 12
let subset: Subset = Subset(values: [1, 2, 3], setSize: setSize)
let r = rank(subset: subset)
let u = unrank(rank: r, setSize: setSize)

print(
        "subset: \(subset)",
        "rank: \(r)",
        "unrank: \(u)",
        separator: "\n"
)

print("\n")

let next = successor(subset: subset)
let nextRank = rank(subset: next)


print(
        "next subset: \(next)",
        "next rank: \(nextRank)",
        separator: "\n"
)

print("\n")

let previous = predecessor(subset: subset)
let previousRank = rank(subset: previous)

print(
        "previous subset: \(previous)",
        "previous rank: \(previousRank)",
        separator: "\n"
)
print("\n")


print("Generate all subsets with `unrank`...")
let subsetsCount: Int = Int(pow(2.0, Double(setSize)))

for rank in 0..<subsetsCount {
    let tempSubset = unrank(rank: rank, setSize: setSize)
    let values = tempSubset.values
    print("rank: \(rank), subset: \(tempSubset), count: \(values.count)")
}

print("------------")
print("Generate all subsets with `successor`...")

var tmpSubset = unrank(rank: 0, setSize: setSize)
print("0: \(tmpSubset)")
for i in 1..<subsetsCount {
    tmpSubset = successor(subset: tmpSubset)
    print("\(i): \(tmpSubset)")
}

//
// Created by Arkadiusz Żmudzin on 2019-05-29.
// Copyright (c) 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

struct Subset: CustomStringConvertible {
    let values: Set<Int>
    let setSize: Int

    init(values: [Int], setSize: Int) {
        self.values = Set(values)
        self.setSize = setSize
    }

    func grayCode() -> [Int] {
        var code: [Int] = []
        for i in 1...setSize {
            code.append(self.values.contains(i) ? 1 : 0)
        }
        return code
    }

    static func fromGrayCode(_ code: [Int]) -> Subset {
        var values: [Int] = []
        for i in 1...code.count {
            if code[i - 1] == 1 {
                values.append(i)
            }
        }

        return Subset(values: values, setSize: code.count)
    }

    var description: String {
        return "\(self.values) (Gray code: \(self.grayCode().map({ val in String(val) }).joined()))"
    }
}

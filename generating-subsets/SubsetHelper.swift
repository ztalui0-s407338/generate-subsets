//
// Created by Arkadiusz Żmudzin on 2019-05-29.
// Copyright (c) 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

func rank(subset: Subset) -> Int {
    let n = subset.setSize
    var rank: Int = 0
    var b: Int = 0

    for index in stride(from: n, through: 0, by: -1) {
        let i: Int = n - index
        if subset.values.contains(i) {
            b = 1 - b;
        }
        rank = 2 * rank + b;
    }

    return rank;
}

func unrank(rank: Int, setSize: Int) -> Subset {
    var subsetValues: [Int] = []
    var tempB = rank % 2
    var tempRank = rank / 2

    for i in 0 ... setSize - 1 {
        let b = tempRank % 2
        if b != tempB {
            subsetValues.append(setSize - i)
        }
        tempB = b
        tempRank = tempRank / 2
    }

    return Subset(values: subsetValues.sorted(), setSize: setSize)
}

func successor(subset: Subset) -> Subset {
    return generateNeighbour(subset: subset, next: true)
}

func predecessor(subset: Subset) -> Subset {
    return generateNeighbour(subset: subset, next: false)
}

private func generateNeighbour(subset: Subset, next: Bool) -> Subset {
    let code = subset.grayCode()

    let weight = grayCodeWeight(code: code)
    let remainder = next ? 0 : 1

    if weight % 2 == remainder {
        return Subset.fromGrayCode(flipBit(at: subset.setSize, grayCode: code))
    } else {
        var j = subset.setSize
        while !subset.values.contains(j) && j > 0 {
            j = j-1
        }
        if j == 1 {
            return Subset(values: [], setSize: subset.setSize)
        } else {
            return Subset.fromGrayCode(flipBit(at: j - 1, grayCode: code))
        }
    }
}

private func grayCodeWeight(code: [Int]) -> Int {
    return code.filter({ $0 == 1}).count
}

private func flipBit(at index: Int, grayCode: [Int]) -> [Int] {
    return grayCode.enumerated().map({ i, element in
        if i != index - 1 {
            return element
        } else {
            return element == 0 ? 1 : 0
        }
    })
}